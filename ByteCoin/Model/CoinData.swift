//
//  CoinData.swift
//  ByteCoin
//
//  Created by Leah Joy Ylaya on 12/14/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct CoinData: Decodable {
    let rate: Double
}
